#!/usr/bin/env python
#Server Connection to MySQL:

import MySQLdb
import time
import os
server_mysql = "127.0.0.1"
server_local_ip = "10.0.0.3"
server_port = "3306"
local_port = "3307" #local port number which receives port forwarding from server on 10.0.0.3

#terminate services using the local port

#setup local port forwarding
os.system("ssh -n -N -f -L %s:localhost:%s %s"%(local_port,server_port,server_local_ip))

#can now connect to mysqldb using localhost (this computer) via port 3307 (on this computer) because port 3306 of 10.0.0.3 is forwarded to here
conn = MySQLdb.connect(host=server_mysql,port=int(local_port),user="root",passwd="plaastech69",db="field_data")
c = conn.cursor()

try:
   c.execute("""INSERT INTO measurements (water_level_sensor_voltage,datetime,sensor_id) VALUES (%s,%s,%s)""",(2.5,time.strftime('%Y-%m-%d %H:%M:%S'),"000000002"))
   conn.commit()
   print "looks like it worked"
except:
   print "dont think it worked"
   conn.rollback()

conn.close()
